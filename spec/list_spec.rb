require 'question/simple_choice/list'
require 'question/simple_choice/codigo'

module Question
  class List
    
    describe Question::List do
           before :each do  
	  
	      # create a Struct with :value and :next
	      Node = Struct.new(:value, :next) #define una estructura indicando los campos que contendrá
	      	      
	      @p1=Question::SimpleChoice.new(
					:text => 'Salida class Xyz \n def pots\n
					@nice\n end\nend\n',:right => 'nil',
 				 	:distractor => ['#<Xyz:0x00000002bf0ed0>',0, "ninguna de las anteriores" ])

	      @node1 = Node.new(@p1,nil)
	      
	      
	      @p2=Question::SimpleChoice.new(
					:text => 'Salida class de hash_raro = {[1, 2, 3] => Object.new(),Hash.new => :toto}', :right => 'Cierto',
 				 	:distractor => ["Falso"])

	      @node2 = Node.new(@p2,nil)
	      
	      
	      @p3=Question::SimpleChoice.new(
					:text => 'class Array \n 
					def say_hi \n
					HEY! 
					end
					end \n
					p[1,, bob].say_hi',:right => 'Ninguna de las anteriores',
 				 	:distractor => ['1', 'bob', 'HEY!'])	      

	      @node3 = Node.new(@p3,nil)
	      
	      
	      @p4=Question::SimpleChoice.new(
					:text => 'Es apropiado que una clase tablero herede de la clase 
					juego?',:right => 'Falso',
 				 	:distractor => ['Cierto'])

	      @node4 = Node.new(@p4,nil)
	      
	      
	      @p5=Question::SimpleChoice.new(
					:text => 'salida de :
 				        class Objeto \n
					end',:right => 'Una instancia de la clase Class',
 				 	:distractor => ['una constante', 'un objeto', 'ninguna de las anteriores'])

	      @node5 = Node.new(@p5,nil)

	      
	      @lista=Question::List.new(:cabeza,:cola)
	      @lista.push(@node1)
	      @lista.push(@node2)
	      @lista.push(@node3)
	      @lista.push(@node4)
	      @lista.push(@node5)
	      
           end
	   
	    context "Pruebas para Node" do
	       it "Debe existir un Nodo de la lista con sus datos y su siguiente" do
	         expect(@lista.cabeza != nil)
	       end
	    end
	   
	   context "Pruebas para List" do
	      it "Se extrae el primer elemento de la lista" do
		  expect(@lista).to respond_to :pop	
	      end
	      
	      it "Se puede insertar un elemento" do
		  expect(@lista).to respond_to :push		  
	      end
	      
	      it "Se pueden insertar varios elementos" do
		  
	      end
	      
	      
	      it "Debe existir una Lista con su cabeza" do

	      end
	   end 
   
    end #fin del before
    
    
  end
end