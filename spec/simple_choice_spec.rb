require 'question/simple_choice'

module Question
  class SimpleChoice
    
    
    before :each do
	  
	  
	  describe Question::SimpleChoice do
	      @q = Question::SimpleChoice.new(:text => '2+2 =', :right => 4, :distractor => [9,3,1])
           end
	   
	   
	   context "Pruebas para Simple_Selection" do
	     
	      it "Debe existir una pregunta" do
		expect(@q.text) == '2+2 ='
		  
	      end
	      
	      it "Se debe invocar a un metodo para obtener la pregunta" do
		  expect(@q).to respond_to :mostrar_pregunta
	      end
	      
	      
	      it "Deben existir opciones de respuesta" do
		  expect(@q.right) == 4
	      end
	      
	      it "Tiene que tener un array de valores falsos" do
		  expect(@q.distractor) == [9,3,1]
	      end
	      
	      it "Se deben mostrar por la consola formateada la pregunta y las opciones de respuesta" do
		  expect(@q).to respond_to :to_s
	      end
	      
	      
	   end
	   
	   
    end #fin del before
    
    
  end
end