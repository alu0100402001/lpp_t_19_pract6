module Question
  class SimpleChoice
    
    attr_accessor :text, :right, :distractor
    
    def initialize(args)
      @text = args[:text]
      raise ArgumentError, 'Specify :text' unless @text
      @right = args[:right]
      raise ArgumentError, 'Specify :right' unless @right
      @distractor = args[:distractor]
      raise ArgumentError, 'Specify :distractor' unless @distractor
    end
    
    
    def to_html
      options = @distractor+[@right]
      options = options.shuffle # para mezclar el array para que no este la correcta siempre al final
      s = ' '
      options.each do |options|
	s += %Q{<input type = "radio" value= "#{options}" name = 0 > #{options}\n}
      end 
      "#{@text}<br/>\n#{s}\n" 
    end
    
    def mostrar_pregunta
       puts @text
    end
    
    def to_s
      opcion = @distractor+[@right]
      #opcion = opcion.shuffle
      s= ' '
      opcion.each do |opcion|
	
	s += %Q{#{opcion}\n}
	
      end
	 "#{@text}\n#{s}\n"
    end
    
  end
end


if __FILE__ == $0 then #si se utiliza desde un require estas lineas no se ejecutan pero si lo ejecutamos en consola funciona para ejecutar 
  qq = Question::SimpleChoice.new(text: 'Cuanto es 2+2 ?', right: 4, distractor: [9,3,1])
  puts qq.to_s
  # puts qq.to_html
  
end